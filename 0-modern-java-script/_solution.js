function* linkGen() {
  const prefix = '/blog/';
  yield prefix;
  for (let i = 2; i <= 40; ++i) {
    yield `${prefix}page/${i}`;
  }
}

async function checkPage(link, check) {
  try {
    const response = await fetch(link);
    const text = await response.text();
    const value = text.includes(check);
    return value && link;
  } catch (e) {
    return false;
  }
}

async function checkFor(text) {
  const pages = await Promise.all([...linkGen()].map(link => checkPage(link, text)));
  const result = pages.map((value, key) => value).filter(p => p)
  console.info(result);
  return result;
}

checkFor('Scala');
