//    Classes
/*****************************************************************************/

class Fuu {
  constructor(x) { this.x = x; }
  toString() { return `x: ${this.x}` }
}

class Bass extends Fuu {
  constructor(y, x) {
    super(x);
    this.x = x * y;
  }
 static sayHi() {
   return 'my name is Bass';
 }
}
b = new Bass(1, 5);
b.toString()
Bass.sayHi()


//    Class properties
/*****************************************************************************/

let work = 'go';
class Fis {
  x = 20;
  [work] = 1;
  static x = 10;
  print = () => `${this.x}`;
  printY() { return `${this.x}` }
}
fis = new Fis();
Fix.x // 10
fis.go // 1
fis.print() === fis.printY()
noFis = {
  print: fis.print,
  printY: fis.printY,
}
noFis.print() // 20
noFis.printY() // Error


//    Class properties
/*****************************************************************************/

function letTest() {
  let x = 31;
  if (true) {
    console.log(x);  // Error
    let x = 71;
  }
  console.log(x);  // 31
}
const fzz = 10;
fzz = 11; // Error


//   const/let
/*****************************************************************************/

function letTest() {
  let x = 31;
  if (true) {
    console.log(x);  // Error
    let x = 71;
  }
  console.log(x);  // 31
}
const fzz = 10;
fzz = 11; // Error

//  arrow functions
/*****************************************************************************/

() => 10;
() => { n: 10 }
() => ({ n: 10 })
() => { return { n: 10 }}

t1 = {
  _x: 1,
  y: function() { return this._x; }
}

t2 = {
  _x: 1,
  y: () => this._x
}

t.y() // 1
t2.y() // undefined

//  Destructuring Assignment
/*****************************************************************************/

let list = [‘one’, ‘two’, ‘fuu’]
let [,second] = list
let [a,b,c,d] = list

[a,b] = [b,a]

let obj = { x: { y: 10 }, z: {} }
const { x } = obj
const { x: { y } } = obj
const  { x: { y: newY } } = obj


// Default arguments
/*****************************************************************************/

function (x = 10) { return x }

(x, b = true) => b && x

({ fuu = 0 }) => fuu

({ fuu = 0 } = {}) => fuu

([fuu = 0, bas]) => fuu

[q1, q2=2] = [ 1 ]

{ q3=3 } = {}


//  other goodies
/*****************************************************************************/

let x = ‘fuu’;
let y = [1,2,3];

let obj1 = { x, y, [x]: y, bar() {} };

let y0 = [0, ...y]

let sum = (...args) =>   args.reduce((s,v) => s+v, 0)

let obj2 = { v: ‘fix’, ...obj1, y: null }

let { fuu, ...obj3 } = obj1

//  iterators
/*****************************************************************************/

let onesIterator = {
  next() {
    return { value: 1, done: false };
  }
}

let iterableOnes = {
  [Symbol.iterator]() {
    return onesIterator;
  }
}

//  generators
/*****************************************************************************/

function* onesGenrator() {
  while(true) { yield 1; }
}

function* superGenerator(limit) {
  yield 0;
  let x = 1;
  while(x < limit) {
    yield x++;
  }
}

((onesGenrator())[Symbol.iterator]()).next() // { value: 1, done: false }

function* sumAndReturn(init = 0) {
  let val = init;
  while(true) { val += yield val; }
}

function* wombo(list) {
  yield* list;
}

function* combo() {
  const fuu = wombo([1,2,3]);
  const bas = sumAndReturn();
  let fuuNext = fuu.next();
  while(!fuuNext.done){
    yield bas.next(fuuNext.value).value;
    fuuNext = fuu.next()
  }
}

[...combo()] // [0, 2, 5]

for (let i of combo()) {
  console.info(i);
}

//  promises
/*****************************************************************************/

let promise = new Promise(
  (resolve, reject) => {
    setTimeout(() => resolve(10), 0)
  }
);

Promise.resolve(10)
  .then(v => v + 10)
  .then(v =>
    new Promise((res) => res(v+10))
  )
  .then(v => Promise.reject(v))
  .catch(v => console.info(v))
//.finnaly(() => {}) currently at stage3
// 30

Promise.all([
  Promise.resolve(1),
  Promise.resolve(2),
]); // [1,2]

// Async/Await
/*****************************************************************************/

async function newWay() {
  let x = await Promise.resolve(10);
  x += 10;
  const fis = () => new Promise(r => r(x + 10))
  x = await fis()
  await Promise.reject(x)
}

newWay().catch(v => console.info(v))

//  fetch
/*****************************************************************************/

fetch(‘http://google.com’)
  .then(resp => resp.text())
  .then(doc => console.info(doc))
  .catch(() => console.info(‘sorry’));
fetch(url, { method: ‘POST’ });


