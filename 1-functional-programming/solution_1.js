listAll = (data = []) =>
  flatten(flatMap(data, node => [node, listAll(node.children)]))

aggegateIds = data =>
  reduce(data, (acc, { id }) => ({...acc, [id]: (acc[id] || 0)+1}), {})

getDuplicatedIds = pipe(listAll, aggegateIds,
  obj => Object.entries(obj),
  data => filter(data, ([,value]) => value > 1),
  data => map(data, ([key]) => Number(key))
)

clean = (data, ids) => pipe(
  data => filter(data, ({id}) => !ids.includes(id)),
  data => map(data, ({ children, ...node}) =>
    !children ? node : { ...node, children: clean(children, ids) })
)(data);

solution = data => clean(data, getDuplicatedIds(data));
