const getInt = max => Math.floor(Math.random() * Math.floor(max));
const getId = () => Math.round(Math.random() * 100);
const getType = () => getInt(4) ? 'ITEM' : 'GROUP';
const getDefaultExpand = () => !getInt(3);
const getExpanable = () => !getInt(3);

const createNode = () =>  ({ id: getId(), type: getType(), expandable: getExpanable(), defaultExpand: getDefaultExpand() });
const generateNodes = count => [...Array(count)].map(createNode);

const fillChildren = (node, limit) => {
  return (limit < 1 || getInt(2)) ? node : {
  ...node,
  children: generateNodes(getInt(5)).map(n => fillChildren(n, limit - 1)),
};
}

const data = generateNodes(8).map(node => fillChildren(node, 3));

function* cursor(nodeList) {
  for (let node of nodeList) {
    yield node;
    if (node.children) {
      yield* cursor(node.children);
    }
  }
}

function* map(itr, fn) {
  for (let item of itr) {
    yield fn(item);
  }
}

function* filter(itr, fn) {
  for (let item of itr) {
    if (fn(item)) {
      yield item;
    }
  }
}

function* reduce(itr, fn, init) {
  yield [...itr].reduce(fn, init);
}

let iterableNodes = [...cursor(data)];

function getDuplicatedIds(nodeList) {
  return Object.entries(nodeList.map(({id}) => id).reduce((a,i) => ({ ...a, [i]: 1 + (a[i] || 0) }), {}))
    .filter(([,value]) => value !== 1)
    .map(([key]) => Number(key))
}

function remove(nodeList, toRemove) {
  return nodeList
    .filter(({ id }) => !toRemove.includes(id))
    .map(node => !node.children ? node : ({ ...node, children: remove(node.children, toRemove) }));
}
const ids = getDuplicatedIds(data);

const newData = remove(data, ids);

const newIds = getDuplicatedIds(newData);

console.info(data);
console.info("'''''''''''''''\n''''''''''''''''''''");
console.info(newData);
console.info(ids, newIds );
console.info("'''''''''''''''\n''''''''''''''''''''");
console.log(JSON.stringify(data));
