listAll = (data = []) =>
  data.map(n => [n, listAll(n.children)]).reduce((a,l) => a.concat(...l), [])
aggegateIds = data =>
  data.reduce((acc, { id }) => ({...acc, [id]: (acc[id] || 0)+1}), {})

getDuplicatedIds = data => [
  listAll,
  aggegateIds,
  obj => Object.entries(obj),
  data => filter(data, ([,value]) => value > 1),
  data => map(data, ([key]) => Number(key))
].reduce((ret, fn) => fn(ret), data);

clean = (data, ids) => data
  .filter(({id}) => !ids.includes(id))
  .map(({ children, ...node}) =>
    !children ? node : { ...node, children: clean(children, ids) })

solution = data => clean(data, getDuplicatedIds(data));

