function map(list, fn) {
  let ret = Array(list.length);
  for (let i = 0; i < list.length; ++i) {
    ret[i] = fn(list[i], i);
  }
  return ret;
}

function filter(list, fn) {
  let ret = []
  for (let i = 0; i < list.length; ++i) {
    if(fn(list[i])) { ret.push(list[i]); }
  }
  return ret;
}

function reduce(list, fn, init) {
  let acc = init;
  for (let i = 0; i < list.length; ++i) {
    acc = fn(acc, list[i]);
  }
  return acc;
}

function reduceRight(list, fn, init) {
  let acc = init;
  for (let i = list.length - 1; i >=0; --i) {
    acc = fn(acc, list[i]);
  }
  return acc;
}

const compose = (...fns) => value =>
  reduceRight(fns, (v, fn) => fn(v), value);


const pipe = (...fns) => value =>
  reduce(fns, (val,fn) => fn(val), value);

const flatten = list => reduce(list, (l,i) => l.concat(i), [])

const flatMap = (list, fn) =>
  compose(flatten, _ => map(_, fn))(list);

const revert = list => reduceRight(list, i => i, []);
